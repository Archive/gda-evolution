/* GDA Evolution Provider
 * Copyright (C) 2003 Rodrigo Moya
 *
 * AUTHORS:
 *         Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-i18n.h>
#include "gda-calendar-model.h"

static void gda_calendar_model_class_init (GdaCalendarModelClass *klass);
static void gda_calendar_model_init (GdaCalendarModel *calendar_model, GdaCalendarModelClass *klass);
static void gda_calendar_model_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

/* These 2 arrays contain the definition of the fields returned by the
   calendar and task models. If you add new fields here, be sure to add
   them also to the data model in task_to_value_list/event_to_value_list */

static const GdaFieldAttributes calendar_cols[] = {
	{ 0, N_("UID"), N_("calendar"), N_("UID"), 0, GDA_VALUE_TYPE_STRING,
	  FALSE, TRUE, TRUE, NULL, FALSE, 0, 0, 0 /* position */ },
	{ 0, N_("summary"), N_("calendar"), N_("Summary"), 0, GDA_VALUE_TYPE_STRING,
	  FALSE, FALSE, FALSE, NULL, FALSE, 0, 0, 1 /* position */ },
	{ 0, N_("location"), N_("calendar"), N_("Location"), 0, GDA_VALUE_TYPE_STRING,
	  TRUE, FALSE, FALSE, NULL, FALSE, 0, 0, 2 /* position */ },
	{ 0, N_("dtstart"), N_("calendar"), N_("Start time"), 0, GDA_VALUE_TYPE_TIMESTAMP,
	  FALSE, FALSE, FALSE, NULL, FALSE, 0, 0, 3 /* position */ },
	{ 0, N_("dtend"), N_("calendar"), N_("End time"), 0, GDA_VALUE_TYPE_TIMESTAMP,
	  FALSE, FALSE, FALSE, NULL, FALSE, 0, 0, 4 /* position */ },
	{ 0, N_("description"), N_("calendar"), N_("Description"), 0, GDA_VALUE_TYPE_STRING,
	  TRUE, FALSE, FALSE, NULL, FALSE, 0, 0, 5 /* position */ },
	{ 0, N_("classification"), N_("calendar"), N_("Classification"), 0, GDA_VALUE_TYPE_STRING,
	  TRUE, FALSE, FALSE, NULL, FALSE, 0, 0, 6 /* position */ },
	{ 0, N_("transparency"), N_("calendar"), N_("Transparency"), 0, GDA_VALUE_TYPE_STRING,
	  TRUE, FALSE, FALSE, NULL, FALSE, 0, 0, 7 /* position */ },
	{ 0, N_("categories"), N_("calendar"), N_("Categories"), 0, GDA_VALUE_TYPE_STRING,
	  TRUE, FALSE, FALSE, NULL, FALSE, 0, 0, 3 /* position */ },

	/* FIXME: alarms */
	/* FIXME: recurrences */
	/* FIXME: meetings */
};
static const GdaFieldAttributes tasks_cols[] = {
	{ 0, N_("UID"), N_("tasks"), N_("UID"), 0, GDA_VALUE_TYPE_STRING,
	  FALSE, TRUE, TRUE, NULL, FALSE, 0, 0, 0 /* position */ },
	{ 0, N_("summary"), N_("tasks"), N_("Summary"), 0, GDA_VALUE_TYPE_STRING,
	  FALSE, FALSE, FALSE, NULL, FALSE, 0, 0, 1 /* position */ },
	{ 0, N_("due"), N_("tasks"), N_("Due date"), 0, GDA_VALUE_TYPE_TIMESTAMP,
	  FALSE, FALSE, FALSE, NULL, FALSE, 0, 0, 2 /* position */ },
	{ 0, N_("dtstart"), N_("tasks"), N_("Start date"), 0, GDA_VALUE_TYPE_TIMESTAMP,
	  FALSE, FALSE, FALSE, NULL, FALSE, 0, 0, 3 /* position */ },
	{ 0, N_("description"), N_("tasks"), N_("Description"), 0, GDA_VALUE_TYPE_STRING,
	  TRUE, FALSE, FALSE, NULL, FALSE, 0, 0, 4 /* position */ },
	{ 0, N_("classification"), N_("calendar"), N_("Classification"), 0, GDA_VALUE_TYPE_STRING,
	  TRUE, FALSE, FALSE, NULL, FALSE, 0, 0, 5 /* position */ },
	{ 0, N_("categories"), N_("tasks"), N_("Categories"), 0, GDA_VALUE_TYPE_STRING,
	  TRUE, FALSE, FALSE, NULL, FALSE, 0, 0, 6 /* position */ },

	/* FIXME: details */
	/* FIXME: assigning */
};

static GdaFieldAttributes *
gda_calendar_model_describe_column (GdaDataModel *model, gint col)
{
	GdaCalendarModel *calendar_model = (GdaCalendarModel *) model;

	g_return_val_if_fail (GDA_IS_CALENDAR_MODEL (calendar_model), NULL);

	switch (calendar_model->obj_type) {
	case CALOBJ_TYPE_EVENT :
		if (col < G_N_ELEMENTS (calendar_cols))
			return gda_field_attributes_copy (&calendar_cols[col]);
		break;
	case CALOBJ_TYPE_TODO :
		if (col < G_N_ELEMENTS (tasks_cols))
			return gda_field_attributes_copy (&tasks_cols[col]);
		break;
	}

	return NULL;
}

static gboolean
gda_calendar_model_is_editable (GdaDataModel *model)
{
	GdaCalendarModel *calendar_model = (GdaCalendarModel *) model;

	g_return_val_if_fail (GDA_IS_CALENDAR_MODEL (calendar_model), FALSE);

	return TRUE;
}

static const GdaRow *
gda_calendar_model_append_row (GdaDataModel *model, const GList *values)
{
}

static gboolean
gda_calendar_model_remove_row (GdaDataModel *model, const GdaRow *row)
{
}

static gboolean
gda_calendar_model_update_row (GdaDataModel *model, const GdaRow *row)
{
}

static void
gda_calendar_model_class_init (GdaCalendarModelClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GdaDataModelClass *model_class = GDA_DATA_MODEL_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_calendar_model_finalize;
	model_class->describe_column = gda_calendar_model_describe_column;
	model_class->is_editable = gda_calendar_model_is_editable;
	model_class->append_row = gda_calendar_model_append_row;
	model_class->remove_row = gda_calendar_model_remove_row;
	model_class->update_row = gda_calendar_model_update_row;
}

static void
gda_calendar_model_init (GdaCalendarModel *calendar_model, GdaCalendarModelClass *klass)
{
}

static void
gda_calendar_model_finalize (GObject *object)
{
	GdaCalendarModel *calendar_model = (GdaCalendarModel *) object;

	if (calendar_model->cal_client) {
		g_object_unref (calendar_model->cal_client);
		calendar_model->cal_client = NULL;
	}

	if (calendar_model->uids) {
		cal_obj_uid_list_free (calendar_model->uids);
		calendar_model->uids = NULL;
	}

	parent_class->finalize (object);
}

GType
gda_calendar_model_get_type (void)
{
        static GType type = 0;
                                                                                                  
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GdaCalendarModelClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gda_calendar_model_class_init,
                        NULL,
                        NULL,
                        sizeof (GdaCalendarModel),
                        0,
                        (GInstanceInitFunc) gda_calendar_model_init
                };
                type = g_type_register_static (GDA_TYPE_DATA_MODEL_ARRAY,
                                               "GdaCalendarModel",
                                               &info, 0);
        }
        return type;
}

static GdaValue *
value_from_date_time (CalComponentDateTime *dt)
{
	GdaTimestamp ts;

	if (dt->value) {
		ts.year = dt->value->year;
		ts.month = dt->value->month;
		ts.day = dt->value->day;
		ts.hour = dt->value->hour;
		ts.minute = dt->value->minute;
		ts.second = dt->value->second;
	} else {
		ts.year = ts.month = ts.day = ts.hour = ts.minute = ts.second = 0;
	}
	ts.fraction = 0;
	ts.timezone = 0;

	return gda_value_new_timestamp ((const GdaTimestamp *) &ts);
}

static GList *
event_to_value_list (GdaCalendarModel *calendar_model, CalComponent *comp)
{
	const char *str;
	CalComponentDateTime dt;
	CalComponentText text;
	GSList *sl;
	CalComponentClassification classif;
	CalComponentTransparency transp;
	GList *value_list = NULL;

	/* the UID */
	cal_component_get_uid (comp, &str);
	if (!str || !*str)
		return NULL;

	value_list = g_list_append (value_list, gda_value_new_string (str));

	/* the Summary */
	cal_component_get_summary (comp, &text);
	value_list = g_list_append (value_list, gda_value_new_string (text.value));

	/* the Location */
	cal_component_get_location (comp, &str);
	value_list = g_list_append (value_list, gda_value_new_string (str));

	/* the Start time */
	cal_component_get_dtstart (comp, &dt);
	value_list = g_list_append (value_list, value_from_date_time (&dt));

	/* the End time */
	cal_component_get_dtend (comp, &dt);
	value_list = g_list_append (value_list, value_from_date_time (&dt));

	/* the Description */
	cal_component_get_description_list (comp, &sl);
	if (sl != NULL) {
		GSList *node;
		GString *s = g_string_new ("");;

		for (node = sl; node != NULL; node = node->next) {
			CalComponentText *ptext = node->data;

			if (ptext && ptext->value)
				s = g_string_append (s, ptext->value);
		}

		value_list = g_list_append (value_list, gda_value_new_string (s->str));

		cal_component_free_text_list (sl);
		g_string_free (s, TRUE);
	} else
		value_list = g_list_append (value_list, gda_value_new_string (""));

	/* the Classification */
	cal_component_get_classification (comp, &classif);
	switch (classif) {
	case CAL_COMPONENT_CLASS_PUBLIC :
		value_list = g_list_append (value_list, gda_value_new_string (_("Public")));
		break;
	case CAL_COMPONENT_CLASS_PRIVATE :
		value_list = g_list_append (value_list, gda_value_new_string (_("Private")));
		break;
	case CAL_COMPONENT_CLASS_CONFIDENTIAL :
		value_list = g_list_append (value_list, gda_value_new_string (_("Confidential")));
		break;
	default :
		value_list = g_list_append (value_list, gda_value_new_string (""));
		break;
	}

	/* the Transparency */
	cal_component_get_transparency (comp, &transp);
	value_list = g_list_append (value_list, gda_value_new_string ("")); /* FIXME */

	/* the Categories */
	cal_component_get_categories (comp, &str);
	value_list = g_list_append (value_list, gda_value_new_string (str));

	return value_list;
}

static GList *
task_to_value_list (GdaCalendarModel *calendar_model, CalComponent *comp)
{
	const char *str;
	GSList *sl;
	CalComponentText text;
	CalComponentDateTime dt;
	CalComponentClassification classif;
	GList *value_list = NULL;

	/* the UID */
	cal_component_get_uid (comp, &str);
	if (!str || !*str)
		return NULL;

	value_list = g_list_append (value_list, gda_value_new_string (str));

	/* the Summary */
	cal_component_get_summary (comp, &text);
	value_list = g_list_append (value_list, gda_value_new_string (text.value));

	/* the Due date */
	cal_component_get_due (comp, &dt);
	value_list = g_list_append (value_list, value_from_date_time (&dt));

	/* the Start time */
	cal_component_get_dtstart (comp, &dt);
	value_list = g_list_append (value_list, value_from_date_time (&dt));

	/* the Description */
	cal_component_get_description_list (comp, &sl);
	if (sl != NULL) {
		GSList *node;
		GString *s = g_string_new ("");;

		for (node = sl; node != NULL; node = node->next) {
			CalComponentText *ptext = node->data;

			if (ptext && ptext->value)
				s = g_string_append (s, ptext->value);
		}

		value_list = g_list_append (value_list, gda_value_new_string (s->str));

		cal_component_free_text_list (sl);
		g_string_free (s, TRUE);
	} else
		value_list = g_list_append (value_list, gda_value_new_string (""));

	/* the Classification */
	cal_component_get_classification (comp, &classif);
	switch (classif) {
	case CAL_COMPONENT_CLASS_PUBLIC :
		value_list = g_list_append (value_list, gda_value_new_string (_("Public")));
		break;
	case CAL_COMPONENT_CLASS_PRIVATE :
		value_list = g_list_append (value_list, gda_value_new_string (_("Private")));
		break;
	case CAL_COMPONENT_CLASS_CONFIDENTIAL :
		value_list = g_list_append (value_list, gda_value_new_string (_("Confidential")));
		break;
	default :
		value_list = g_list_append (value_list, gda_value_new_string (""));
		break;
	}

	/* the Categories */
	cal_component_get_categories (comp, &str);
	value_list = g_list_append (value_list, gda_value_new_string (str));

	return value_list;
}

static void
load_objects (GdaCalendarModel *calendar_model)
{
	GList *l;

	for (l = calendar_model->uids; l != NULL; l = l->next) {
		CalComponent *comp;
		CalClientGetStatus status;
		gchar *uid = l->data;
		GList *value_list = NULL;

		/* retrieve the object from the calendar backend */
		status = cal_client_get_object (calendar_model->cal_client,
						uid, &comp);
		if (status != CAL_CLIENT_GET_SUCCESS) {
			gda_data_model_array_clear (GDA_DATA_MODEL_ARRAY (calendar_model));
			return;
		}

		/* add the component to the data model */
		if (calendar_model->obj_type == CALOBJ_TYPE_TODO)
			value_list = task_to_value_list (calendar_model, comp);
		else if (calendar_model->obj_type == CALOBJ_TYPE_EVENT)
			value_list = event_to_value_list (calendar_model, comp);

		if (value_list != NULL) {
			GdaRow *row;

			row = GDA_DATA_MODEL_CLASS (parent_class)->append_row (
				GDA_DATA_MODEL (calendar_model), value_list);
			if (row != NULL)
				gda_row_set_id (row, uid);

			g_list_foreach (value_list, (GFunc) gda_value_free, NULL);
			g_list_free (value_list);
		}

		g_object_unref (comp);
	}
}

GdaDataModel *
gda_calendar_model_new (CalClient *cal_client, CalObjType obj_type)
{
	GdaCalendarModel *calendar_model;
	gint i;

	g_return_val_if_fail (IS_CAL_CLIENT (cal_client), NULL);

	calendar_model = g_object_new (GDA_TYPE_CALENDAR_MODEL, NULL);
	calendar_model->obj_type = obj_type;
	if (obj_type == CALOBJ_TYPE_EVENT) {
		gda_data_model_array_set_n_columns (GDA_DATA_MODEL_ARRAY (calendar_model),
						    G_N_ELEMENTS (calendar_cols));
		for (i = 0; i < G_N_ELEMENTS (calendar_cols); i++) {
			gda_data_model_set_column_title (GDA_DATA_MODEL (calendar_model),
							 i, calendar_cols[i].name);
		}
	} else if (obj_type == CALOBJ_TYPE_TODO) {
		gda_data_model_array_set_n_columns (GDA_DATA_MODEL_ARRAY (calendar_model),
						    G_N_ELEMENTS (tasks_cols));
		for (i = 0; i < G_N_ELEMENTS (tasks_cols); i++) {
			gda_data_model_set_column_title (GDA_DATA_MODEL (calendar_model),
							 i, tasks_cols[i].name);
		}
	} else {
		g_object_unref (calendar_model);
		return NULL;
	}

	calendar_model->cal_client = cal_client;
	g_object_ref (calendar_model->cal_client);

	/* load all objects of the selected type */
	calendar_model->uids = cal_client_get_uids (cal_client, obj_type);
	if (calendar_model->uids)
		load_objects (calendar_model);

	return (GdaDataModel *) calendar_model;
}
