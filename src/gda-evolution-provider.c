/* GDA Evolution Provider
 * Copyright (C) 2003 Rodrigo Moya
 *
 * AUTHORS:
 *         Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgda/gda-data-model-list.h>
#include <libgda/gda-select.h>
#include <libgnome/gnome-i18n.h>
#include "gda-evolution-connection.h"
#include "gda-evolution-provider.h"

static void gda_evolution_provider_class_init (GdaEvolutionProviderClass *klass);
static void gda_evolution_provider_init (GdaEvolutionProvider *provider,
					 GdaEvolutionProviderClass *klass);
static void gda_evolution_provider_finalize (GObject *object);

static const gchar *gda_evolution_provider_get_version (GdaServerProvider *provider);
static gboolean gda_evolution_provider_open_connection (GdaServerProvider *provider,
							GdaConnection *cnc,
							GdaQuarkList *params,
							const gchar *username,
							const gchar *password);
static gboolean gda_evolution_provider_close_connection (GdaServerProvider *provider,
							 GdaConnection *cnc);
static const gchar *gda_evolution_provider_get_server_version (GdaServerProvider *provider,
							       GdaConnection *cnc);
static const gchar *gda_evolution_provider_get_database (GdaServerProvider *provider,
							 GdaConnection *cnc);
static gboolean gda_evolution_provider_create_database (GdaServerProvider *provider,
							GdaConnection *cnc,
							const gchar *name);
static gboolean gda_evolution_provider_drop_database (GdaServerProvider *provider,
						      GdaConnection *cnc,
						      const gchar *name);
static GList *gda_evolution_provider_execute_command (GdaServerProvider *provider,
						      GdaConnection *cnc,
						      GdaCommand *cmd,
						      GdaParameterList *params);
static gboolean gda_evolution_provider_begin_transaction (GdaServerProvider *provider,
							  GdaConnection *cnc,
							  GdaTransaction *xaction);
static gboolean gda_evolution_provider_commit_transaction (GdaServerProvider *provider,
							   GdaConnection *cnc,
							   GdaTransaction *xaction);
static gboolean gda_evolution_provider_rollback_transaction (GdaServerProvider *provider,
							     GdaConnection *cnc,
							     GdaTransaction *xaction);
static gboolean gda_evolution_provider_supports (GdaServerProvider *provider,
						 GdaConnection *cnc,
						 GdaConnectionFeature feature);
static GdaDataModel *gda_evolution_provider_get_schema (GdaServerProvider *provider,
							GdaConnection *cnc,
							GdaConnectionSchema schema,
							GdaParameterList *params);

static GObjectClass *parent_class = NULL;

/*
 * GdaEvolutionProvider class implementation
 */
static void
gda_evolution_provider_class_init (GdaEvolutionProviderClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GdaServerProviderClass *provider_class = GDA_SERVER_PROVIDER_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gda_evolution_provider_finalize;
	provider_class->get_version = gda_evolution_provider_get_version;
	provider_class->open_connection = gda_evolution_provider_open_connection;
	provider_class->close_connection = gda_evolution_provider_close_connection;
	provider_class->get_server_version = gda_evolution_provider_get_server_version;
	provider_class->get_database = gda_evolution_provider_get_database;
	provider_class->create_database = gda_evolution_provider_create_database;
	provider_class->drop_database = gda_evolution_provider_drop_database;
	provider_class->execute_command = gda_evolution_provider_execute_command;
	provider_class->begin_transaction = gda_evolution_provider_begin_transaction;
	provider_class->commit_transaction = gda_evolution_provider_commit_transaction;
	provider_class->rollback_transaction = gda_evolution_provider_rollback_transaction;
	provider_class->supports = gda_evolution_provider_supports;
	provider_class->get_schema = gda_evolution_provider_get_schema;
}

static void
gda_evolution_provider_init (GdaEvolutionProvider *evo_prv, GdaEvolutionProviderClass *klass)
{
}

static void
gda_evolution_provider_finalize (GObject *object)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) object;

	g_return_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv));

	parent_class->finalize (object);
}

GType
gda_evolution_provider_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static GTypeInfo info = {
			sizeof (GdaEvolutionProviderClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gda_evolution_provider_class_init,
			NULL, NULL,
			sizeof (GdaEvolutionProvider),
			0,
			(GInstanceInitFunc) gda_evolution_provider_init
		};
		type = g_type_register_static (GDA_TYPE_SERVER_PROVIDER,
					       "GdaEvolutionProvider",
					       &info, 0);
	}

	return type;
}

GdaServerProvider *
gda_evolution_provider_new (void)
{
	GdaEvolutionProvider *provider;

	provider = g_object_new (gda_evolution_provider_get_type (), NULL);
	return GDA_SERVER_PROVIDER (provider);
}

static const gchar *
gda_evolution_provider_get_version (GdaServerProvider *provider)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), NULL);
	return VERSION;
}

static gboolean
gda_evolution_provider_open_connection (GdaServerProvider *provider,
					GdaConnection *cnc,
					GdaQuarkList *params,
					const gchar *username,
					const gchar *password)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), FALSE);

	return gda_evolution_connection_init (cnc);
}

static gboolean
gda_evolution_provider_close_connection (GdaServerProvider *provider,
					 GdaConnection *cnc)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), FALSE);

	return gda_evolution_connection_dispose (cnc);
}

static const gchar *
gda_evolution_provider_get_server_version (GdaServerProvider *provider,
					   GdaConnection *cnc)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), NULL);

	return EVOLUTION_VERSION;
}

static const gchar *
gda_evolution_provider_get_database (GdaServerProvider *provider,
				     GdaConnection *cnc)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), NULL);

	/* FIXME: we only support the local calendar/contacts. The idea would
	   be to have each configured evolution account as a database, once
	   we can easily access that information */
	return _("local");
}

static gboolean
gda_evolution_provider_create_database (GdaServerProvider *provider,
					GdaConnection *cnc,
					const gchar *name)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), FALSE);

	gda_connection_add_error_string (cnc, _("Creation of databases is not supported yet"));
	return FALSE;
}

static gboolean
gda_evolution_provider_drop_database (GdaServerProvider *provider,
				      GdaConnection *cnc,
				      const gchar *name)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), FALSE);

	gda_connection_add_error_string (cnc, _("Removal of databases is not supported yet"));
	return FALSE;
}

static GdaDataModel *
execute_sql (GdaConnection *cnc, const gchar *sql)
{
	GdaSelect *select;
	GdaDataModel *tables;
	gint r, rows;

	/* get the list of tables */
	tables = gda_evolution_connection_get_tables_schema (cnc, NULL);
	if (!GDA_IS_DATA_MODEL (tables)) {
		gda_connection_add_error_string (cnc, _("Tables schema could not be retrieved"));
		return NULL;
	}

	rows = gda_data_model_get_n_rows (tables);

	/* create the GdaSelect object */
	select = gda_select_new ();
	gda_select_set_sql (GDA_SELECT (select), sql);

	for (r = 0; r < rows; r++) {
		gchar *table_name;
		GdaDataModel *table;

		table_name = gda_value_get_string (gda_data_model_get_value_at (tables, 0, r));
		table = gda_evolution_connection_get_table (cnc, table_name);
		if (table != NULL)
			gda_select_add_source (GDA_SELECT (select),
					       table_name,
					       (const GdaDataModel *) table);
	}

	/* run the SQL */
	if (!gda_select_run (GDA_SELECT (select))) {
		gda_connection_add_error_string (cnc, _("Invalid SQL command"));
		g_object_unref (select);
		select = NULL;
	}

	/* free memory */
	g_object_unref (tables);

	return (GdaDataModel *) select;
}

static GList *
gda_evolution_provider_execute_command (GdaServerProvider *provider,
					GdaConnection *cnc,
					GdaCommand *cmd,
					GdaParameterList *params)
{
	GList *model_list = NULL;
	gchar **commands;
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), NULL);

	/* parse the string */
	commands = g_strsplit (gda_command_get_text (cmd), ";", 0);
	if (commands) {
		gint n;
		GdaDataModel *model;

		for (n = 0; commands[n] != NULL; n++) {
			switch (gda_command_get_command_type (cmd)) {
			case GDA_COMMAND_TYPE_SQL :
				model = execute_sql (cnc, commands[n]);
				if (GDA_IS_DATA_MODEL (model))
					model_list = g_list_append (model_list, model);
				break;
			case GDA_COMMAND_TYPE_TABLE :
				model = gda_evolution_connection_get_table (cnc, commands[n]);
				if (GDA_IS_DATA_MODEL (model))
					model_list = g_list_append (model_list, model);
				break;
			}
		}

		g_strfreev (commands);
	}

	return model_list;
}

static gboolean
gda_evolution_provider_begin_transaction (GdaServerProvider *provider,
					  GdaConnection *cnc,
					  GdaTransaction *xaction)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), FALSE);

	gda_connection_add_error_string (cnc, _("Transactions are not supported"));
	return FALSE;
}

static gboolean
gda_evolution_provider_commit_transaction (GdaServerProvider *provider,
					   GdaConnection *cnc,
					   GdaTransaction *xaction)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), FALSE);

	gda_connection_add_error_string (cnc, _("Transactions are not supported"));
	return FALSE;
}

static gboolean
gda_evolution_provider_rollback_transaction (GdaServerProvider *provider,
					     GdaConnection *cnc,
					     GdaTransaction *xaction)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), FALSE);

	gda_connection_add_error_string (cnc, _("Transactions are not supported"));
	return FALSE;
}

static gboolean
gda_evolution_provider_supports (GdaServerProvider *provider,
				 GdaConnection *cnc,
				 GdaConnectionFeature feature)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), FALSE);

	switch (feature) {
	default :
	}

	return FALSE;
}

static GdaDataModel *
get_databases_schema (GdaConnection *cnc)
{
	GdaDataModelList *model;
	GdaValue *value;

	model = gda_data_model_list_new ();
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 0, _("Name"));

	value = gda_value_new_string (_("local"));
	gda_data_model_list_append_value (model, (const GdaValue *) value);
	gda_value_free (value);

	return (GdaDataModel *) model;
}

static GdaDataModel *
gda_evolution_provider_get_schema (GdaServerProvider *provider,
				   GdaConnection *cnc,
				   GdaConnectionSchema schema,
				   GdaParameterList *params)
{
	GdaEvolutionProvider *evo_prv = (GdaEvolutionProvider *) provider;

	g_return_val_if_fail (GDA_IS_EVOLUTION_PROVIDER (evo_prv), NULL);

	switch (schema) {
	case GDA_CONNECTION_SCHEMA_DATABASES :
		return get_databases_schema (cnc);
	case GDA_CONNECTION_SCHEMA_FIELDS :
		return gda_evolution_connection_get_fields_schema (cnc, params);
	case GDA_CONNECTION_SCHEMA_TABLES :
		return gda_evolution_connection_get_tables_schema (cnc, params);
	case GDA_CONNECTION_SCHEMA_TYPES :
		return gda_evolution_connection_get_types_schema (cnc);
	default :
	}

	return NULL;
}
