/* GDA Evolution Provider
 * Copyright (C) 2003 Rodrigo Moya
 *
 * AUTHORS:
 *         Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gmodule.h>
#include <libgda/gda-config.h>
#include <libgnome/gnome-i18n.h>
#include "gda-evolution-provider.h"

const gchar       *plugin_get_name (void);
const gchar       *plugin_get_description (void);
GList             *plugin_get_connection_params (void);
GdaServerProvider *plugin_create_provider (void);

const gchar *
plugin_get_name (void)
{
	return "Evolution";
}

const gchar *
plugin_get_description (void)
{
	return _("Provider for Ximian Evolution personal data");
}

GList *
plugin_get_connection_params (void)
{
	GList *list = NULL;

	return list;
}

GdaServerProvider *
plugin_create_provider (void)
{
	return gda_evolution_provider_new ();
}

/*
 * Module initialization code, to make sure there is always a data source
 * configured to access Evolution.
 */
const gchar *
g_module_check_init (GModule *module)
{
	GList *dsn_list, *l;
	GdaDataSourceInfo *dsn_info = NULL;;

	dsn_list = gda_config_get_data_source_list ();
	for (l = dsn_list; l != NULL; l = l->next) {
		dsn_info = l->data;
		if (dsn_info && !strcmp (dsn_info->provider, "Evolution"))
			break;

		dsn_info = NULL;
	}

	if (!dsn_info) {
		/* if there was no data source with 'Evolution' as the
		   provider, create a default data source for accessing
		   Evolution's default calendar, contacts and tasks */
		gda_config_save_data_source ("Evolution", "Evolution", "",
					     _("Data source for accessing Evolution's default calendar, tasks and contacts"),
					     NULL, NULL);
	}

	gda_config_free_data_source_list (dsn_list);

	return NULL;
}
